# ![logo](./public/images/logo.png)

![build](https://img.shields.io/badge/build-passing-brightgreen)
![version](https://img.shields.io/badge/version-2.2.0-brightgreen)
![license](https://img.shields.io/badge/license-MIT-brightgreen)

## **Zulu LandingPage** 🔥🔥🔥

## Giới thiệu

Zulu dùng để tạo các video chất lượng cao, giúp bạn giải quyết các vấn đề về thiết kế, tạo video 😄
Luyện tập để nâng cao các kĩ năng `DESIGN`, `UI/UX` ngay nào 😍

## Sản phẩm lấy ý tưởng trên youtube

Channel: [Evondev](https://www.youtube.com/channel/UC8vjHOEYlnVTqAgE6CFDm_Q), anh là một front-end developer chuyên chia sẻ các kiến thức về front-end cho người mới. Các bạn có thể ghé qua ủng hộ anh nha 😘😘😘
Kham khảo khoá Cắt giao diện Zulu LandingPage: [Tại đây](https://www.youtube.com/watch?v=0x9spwyfn9w&list=PLd8OdiciAE1S9PhUtk2H1o8cfqXNhOp5Y)
